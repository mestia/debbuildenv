debbuildenv
=============

This repository contains zsh, vim and screen files for the Grml system and
common files needed for building debian packages with git-buildpackage.

Further information about grml is available from http://grml.org/console/ and https://grml.org/zsh/#grmlzshconfig

See also https://unix.stackexchange.com/questions/58319/what-is-the-key-difference-between-grml-zsh-config-and-oh-my-zsh-config

    apt-get install devscripts quilt git-buildpackage fakeroot curl wget ca-certificates screen zsh
    git clone https://salsa.debian.org/mestia/debbuildenv.git
    ls -1 debbuildenv | grep -v "REDAME" | while read line; do echo debbuildenv/$line  ~/.${line}; done

Substitute `echo` with `cp` or better 'ln -s` in the command above if you really want to overwrite files.
Alternatively pick up the configs one by one

-----

A hack to convert bash_history to zsh [extended](http://zsh.sourceforge.net/Doc/Release/Options.html#History "ZSH history") history:

    export HISTSIZE; perl -ne  'print ": ". (time()-$ENV{'HISTSIZE'}+$.) .":0;".$_' ~/.bash_history >~/.zsh_history

The timestamp is obviously faked, but that doesn't matter, actually it even works with constant timestamp.
